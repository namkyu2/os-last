#include "userprog/syscall.h"
#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>
#include <init.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
/*vaddr because of phys_base */
#include "threads/vaddr.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "userprog/process.h"
#include "threads/palloc.h"
#include "filesys/inode.h"
#include "vm/frame.h"
#include "vm/page.h"

static void syscall_handler (struct intr_frame *);
static int get_user (const uint8_t *);
static bool put_user (uint8_t *, uint8_t);
bool syscall_mmap_less_function(const struct list_elem *, const struct list_elem *, void *);


void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&sys_lock);
}

static void
syscall_handler (struct intr_frame *f UNUSED)
{
	int syscall;
	if (!get_argument(f->esp, &syscall)) goto error;
	// printf("syscall %d in\n", syscall);
	thread_current()->stack_growth_esp = f->esp;
	switch(syscall){
		case SYS_HALT:{
			sys_halt();
			break;
		}
		case SYS_EXIT:{
			int status;
			if (!(get_argument(f->esp+4, &status)))
				goto error;
			sys_exit(status);
			break;
		}
		case SYS_EXEC:{
			const char* cmd_line;
			if (!(get_argument(f->esp+4, &cmd_line)))
				goto error;
			f->eax = sys_exec(cmd_line);
			break;
		}
		case SYS_WAIT:{
			tid_t tid;
			if (!(get_argument(f->esp+4, &tid)))
				goto error;
			f->eax = sys_wait(tid);
			break;
		}
		case SYS_CREATE:{
			const char* file;
			unsigned initial_size;
			if (!(get_argument(f->esp+4, &file) && get_argument(f->esp+8, &initial_size)))
				goto error;
			f->eax = sys_create(file, initial_size);
			break;
		}
		case SYS_REMOVE:{
			const char* file;
			if (!(get_argument(f->esp+4, &file)))
				goto error;
			f->eax = sys_remove(file);
			break;
		}
		case SYS_OPEN:{  /*6666666666666666666666666666*/
			const char* file;
			if (!(get_argument(f->esp+4, &file)))
				goto error;
			f->eax = sys_open(file);
			break;
		}
		case SYS_FILESIZE:{
			int fd;
			if (!(get_argument(f->esp+4, &fd)))
				goto error;
			f->eax = sys_filesize(fd);
			break;
		}
		case SYS_READ:{
			int fd;
			void* buffer;
			unsigned size;
			if (!(get_argument(f->esp+4, &fd) && get_argument(f->esp+8, &buffer) && get_argument(f->esp+12, &size)))
				goto error;
			// printf("check1\n");
			f->eax = sys_read(fd, buffer, size);
			break;
		}
		case SYS_WRITE:{ /*999999999999999999999999999999999999*/
			int fd;
			const void* buffer;
			unsigned size;
			if (!(get_argument(f->esp+4, &fd) && get_argument(f->esp+8, &buffer) && get_argument(f->esp+12, &size)))
				goto error;

			f->eax = sys_write(fd, buffer, size);
			break;
		}
		case SYS_SEEK:{
			int fd;
			unsigned position;
			if (!(get_argument(f->esp+4, &fd)&&get_argument(f->esp+8, &position)))
				goto error;
			sys_seek(fd, position);
			break;
		}
		case SYS_TELL:{
			int fd;
			if (!(get_argument(f->esp+4, &fd)))
				goto error;
			f->eax = sys_tell(fd);
			break;
		}
		case SYS_CLOSE:{
			int fd;
			if (!(get_argument(f->esp+4, &fd)))
				goto error;
			sys_close(fd);
			break;
		}
		case SYS_MMAP:
		{
			int fd;
			void *addr;
			if (!(get_argument(f->esp+4, &fd) && get_argument(f->esp+8, &addr)))
				goto error;
			f->eax = sys_mmap(fd,addr);
			break;
		}
		case SYS_MUNMAP:
		{
			int mapped;
			if (!get_argument(f->esp+4, &mapped))
				goto error;

			sys_munmap(mapped);
			break;
		}
		default:
			break;
	}
	return;
	error:{
		sys_exit(-1);
	}
}
void sys_halt(void)
{
	power_off();
}

void sys_exit(int status)
{
	printf("%s: exit(%d)\n", thread_name(), status);
	thread_current()->cause_of_death = status;
	thread_exit();
}

tid_t sys_exec(const char* cmd_line)
{
	if (!check_string_user(cmd_line))
	{
		sys_exit(-1);
	}

	return process_execute(cmd_line);
}

int sys_wait(tid_t tid)
{
	int result = process_wait(tid);
	return result;
}

bool sys_create(const char* file, unsigned initial_size)
{
	lock_acquire(&sys_lock);
	if(file==NULL)
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}
	if (!check_string_user(file))
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}

	bool result = filesys_create(file, initial_size);
	lock_release(&sys_lock);
	return result;
}

bool sys_remove(const char* file)
{
	lock_acquire(&sys_lock);
	if (file == NULL)
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}

	bool result = filesys_remove(file);
	lock_release(&sys_lock);
	return result;
}

int sys_open(const char* file)
{
	lock_acquire(&sys_lock);
	if (file == NULL)
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}
	if(!check_string_user(file))
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}

	struct file* opened;
	opened = filesys_open(file);
	if (opened==NULL){
		lock_release(&sys_lock);
		return -1;
	}
	int result = fd_create(opened, file);
	lock_release(&sys_lock);
	return result;
}

int sys_filesize(int fd)
{
	lock_acquire(&sys_lock);
	struct file* target = get_file_by_fdnum(fd);

	if (target==NULL){
		lock_release(&sys_lock);
		return -1;
	}

	int result = file_length(target);
	lock_release(&sys_lock);
	return result;
}

int sys_read(int fd, void* buffer, unsigned size)
{
	lock_acquire(&sys_lock);
	// printf("check2\n");
	if (!check_user(buffer, size))
	{
		lock_release(&sys_lock);
		// printf("check3\n");
		sys_exit(-1);
	}
	if (fd == 0)
	{
		int i;
		for(i=0; i<size; i++)
			*(uint8_t*)(buffer+i) = input_getc();
	}
	struct file* target = get_file_by_fdnum(fd);
	// printf("check4\n");
	if (target==NULL){
		lock_release(&sys_lock);
		// printf("check5\n");
		return -1;
	}
	lock_release(&sys_lock);
	int result = (int)file_read(target, buffer, size);
	// printf("check6\n");
	return result;
}

int sys_write(int fd, const void* buffer, unsigned size)
{
	lock_acquire(&sys_lock);
	if (!check_user(buffer, size))
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}

	if(fd == 1)
	{
		putbuf(buffer, size);
		lock_release(&sys_lock);
		return size;
	}

	struct fd* tmp_fd = get_fd_by_fdnum(fd);

	if (tmp_fd == NULL){
		lock_release(&sys_lock);
		return -1;
	}
	
	if (thread_find_filename(tmp_fd->file_name)){
		lock_release(&sys_lock);
		return 0;
	}
	
	int result = file_write(tmp_fd->file, buffer,size);
	lock_release(&sys_lock);
	return result;
}

void sys_seek(int fd, unsigned position)
{
	lock_acquire(&sys_lock);
	struct file* target = get_file_by_fdnum(fd);

	if (target == NULL)
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}

	file_seek(target, position);
	lock_release(&sys_lock);
}

unsigned sys_tell(int fd)
{
	lock_acquire(&sys_lock);
	struct file* target = get_file_by_fdnum(fd);

	if (target == NULL){
		lock_release(&sys_lock);
		return -1;
	}

	unsigned result = file_tell(target);
	lock_release(&sys_lock);
	return result;
}

void sys_close(int fd)
{
	lock_acquire(&sys_lock);
	struct fd* target = get_fd_by_fdnum(fd);

	if (target != NULL)
	{
		file_close(target->file);
		list_remove(&target->fd_elem);
		palloc_free_page(target);
	}
	else
	{
		lock_release(&sys_lock);
		sys_exit(-1);
	}
	lock_release(&sys_lock);
}

int sys_mmap(int fd, void *addr)
{
	// printf("mmap 1\n");
	if(addr == 0 || pg_ofs(addr))
		return -1;
	// printf("mmap 2\n");
	lock_acquire(&sys_lock);
	struct file* target = get_file_by_fdnum(fd);
	int offset;
	int map_num;
	void* temp_addr;
	// printf("mmap 3\n");
	if(target == NULL)
	{
		lock_release(&sys_lock);
		return -1;
	}
	// printf("mmap 4\n");
	target = file_reopen(target);
	if(target == NULL)
	{
		lock_release(&sys_lock);
		return -1;
	}
	// printf("mmap 5\n");
	int size = file_length(target);

	if(size == 0)
	{
		lock_release(&sys_lock);
		return -1;
	}
	// printf("mmap 6\n");
	// printf("mmap 7\n");
	for(temp_addr = addr, offset = 0; offset + PGSIZE < size; offset += PGSIZE, temp_addr += PGSIZE)
	{
		if(!page_table_set_file(thread_current()->page_table, temp_addr, target, offset, PGSIZE,0,true))
		{
			lock_release(&sys_lock);
			return -1;
		}
	}
	// printf("mmap 8\n");
	if(!page_table_set_file(thread_current()->page_table, temp_addr, target, offset, size - offset, PGSIZE - size + offset, true))
	{
		lock_release(&sys_lock);
		return -1;
	}
	// printf("mmap 9\n");
	if(list_empty(&thread_current()->mmap_list))
		map_num = 0;
	else
		map_num = list_entry(list_max(&thread_current()->mmap_list, syscall_mmap_less_function, NULL), struct mmap_struct, elem)->num + 1;

	struct mmap_struct* temp_mmap = malloc(sizeof(struct mmap_struct));
	temp_mmap->num = map_num;
	temp_mmap->file = target;
	temp_mmap->start_addr = addr;
	temp_mmap->end_addr = addr + size;
	// printf("num : %d\n",map_num);
	// printf("file : %d\n", target);
	// printf("start_addr : %d\n", addr);
	// printf("end_addr : %d\n",addr + size);
	// printf("size : %d\n", size);
	// printf("%d\n",*(int*)0xc0000000);
	// hex_dump((int*)0x10000000, (int*)0x10000000, size, true);
	list_push_back(&thread_current()->mmap_list, &temp_mmap->elem);
	// printf("done\n");
	lock_release(&sys_lock);

	return map_num;
}

void sys_munmap(int mapped)
{
	// printf("munmap 1\n");
	lock_acquire(&sys_lock);
	struct list_elem *e;
	struct mmap_struct *mmap;
	bool ret;
	// printf("munmap 2\n");
	for (e = list_begin(&thread_current()->mmap_list);
	 e != list_end(&thread_current()->mmap_list);
	  e = list_next(e))
	{
		// printf("munmap 2.3\n");
		mmap = list_entry(e, struct mmap_struct, elem);
		// printf("munmap 2.5\n");
		if(mapped == mmap->num)
			break;
		// printf("munmap 2.7\n");
	}
	// printf("munmap 3\n");
	if(e == list_end(&thread_current()->mmap_list)){
		lock_release(&sys_lock);
		sys_exit(-1);
	}
	// printf("munmap 4\n");
	page_table_munmap(thread_current()->page_table, thread_current()->pagedir, mmap->file, mmap->start_addr, mmap->end_addr);
	// printf("munmap 5\n");
	list_remove(e);
	free(mmap);
	// printf("munmap 6\n");
	lock_release(&sys_lock);
}

bool syscall_mmap_less_function(const struct list_elem* a, const struct list_elem* b, void *aux)
{
	return (list_entry(a, struct mmap_struct, elem)->num < list_entry(b, struct mmap_struct, elem)->num);

}

/* Reads a byte at user virtual address UADDR.
UADDR must be below PHYS_BASE.
Returns the byte value if successful, -1 if a segfault
occurred. */
static int
get_user (const uint8_t *uaddr)
{	
	// printf("uaddr : %d", uaddr);
	if(uaddr >= PHYS_BASE){
		// printf("check 7\n");
		return -1;
	}

	if(uaddr <= 0x08048000){
		// printf("check8\n");
		return -1;
	}

	// if (!pagedir_get_page(thread_current()->pagedir, uaddr)){
	// 	printf("check9\n");
	// 	return -1;
	// }

	int result;
	asm ("movl $1f, %0; movzbl %1, %0; 1:"
		: "=&a" (result) : "m" (*uaddr));

	return result;
}

/* Writes BYTE to user address UDST.
UDST must be below PHYS_BASE.
Returns true if successful, false if a segfault occurred. */
static bool
put_user (uint8_t *udst, uint8_t byte)
{
	int error_code;
	asm ("movl $1f, %0; movb %b2, %1; 1:"
		: "=&a" (error_code), "=m" (*udst) : "r" (byte));
	return error_code != -1;
}

bool check_user(const uint8_t *uaddr, uint8_t byte){
	/*유저 영역인지 확인, 아니면 exit(-1) 호출 */ 
	int i;
	for (i=0; i<byte; i++)
	{
		if(get_user(uaddr+i)==-1)
			return false;
	}
	return true;
}

bool check_string_user(const char *uaddr)
{
	int i;
	int value;

	for(i = 0; ; i++)
	{
		value = get_user(uaddr + i);

		if (value == -1)
			return false;

		else if (value == '\0')
			break;
	}

	return true;
}

bool get_argument(void* esp, void* arg)
{
	/*esp에서 4B씩 긁어 arg에 저장*/
	int value;
	int i;
	
	if (!check_user(esp, 4))
		return false;

	for(i=0; i<4; i++)
	{
		value = get_user(esp +i);
		put_user(arg+i, value);
	}

	return true;
}

/*moved from process.c*/

int fd_create(struct file* file, char* file_name)
{
  struct fd *fd = palloc_get_page(4);

  if (fd == NULL)
  	return -1;

  fd->file = file;
  fd->file_name = file_name;
  int fd_num = 1;
  int tmp_fd_num;
  struct list_elem* e = NULL;

  for (e = list_begin(&thread_current()->fd_list); e != list_tail(&thread_current()->fd_list); e = list_next(e)) 
  {
    tmp_fd_num = list_entry(e, struct fd, fd_elem)->fd_num;

    if (tmp_fd_num > fd_num)
      fd_num = tmp_fd_num;

  }

  fd_num++;
  fd->fd_num = fd_num;

  list_push_back(&thread_current()->fd_list, &fd->fd_elem);
  return fd_num;
}

struct file* get_file_by_fdnum(int fd_num)
{
	struct fd* fd = get_fd_by_fdnum(fd_num);

	if (fd)
		return fd->file;
	else
		return NULL;	
}

struct fd* get_fd_by_fdnum(int fd_num)
{
	struct list_elem* e = NULL;
	int tmp_fd_num;

  	for (e = list_begin(&thread_current()->fd_list); e != list_tail(&thread_current()->fd_list); e = list_next(e))
  	{
		tmp_fd_num = list_entry(e, struct fd, fd_elem)->fd_num;

		if (tmp_fd_num == fd_num)
		  return list_entry(e, struct fd, fd_elem);
	}

	return NULL;
}