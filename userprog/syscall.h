#include "threads/thread.h"

#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

void syscall_init (void);

void sys_halt(void);
void sys_exit(int);
tid_t sys_exec(const char*);
int sys_wait(tid_t);
bool sys_create(const char*, unsigned);
bool sys_remove(const char*);
int sys_open(const char*);
int sys_filesize(int fd);
int sys_read(int, void*, unsigned);
int sys_write(int, const void*, unsigned);
void sys_seek(int, unsigned);
unsigned sys_tell(int);
void sys_close(int);
int sys_mmap(int , void *);
void sys_munmap(int );

struct mmap_struct
{
  int num;
  struct file* file;

  void* start_addr;
  void* end_addr;

  struct list_elem elem;
};

bool check_user(const uint8_t *, uint8_t);
bool check_string_user(const char *);
bool get_argument(void*, void*);

struct fd{
	int fd_num;
	struct file* file;
	struct list_elem fd_elem;
	char* file_name;
};

static struct lock sys_lock;
int fd_create(struct file*, char*);
struct file* get_file_by_fdnum(int);
struct fd* get_fd_by_fdnum(int);
#endif /* userprog/syscall.h */
