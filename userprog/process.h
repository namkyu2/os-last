#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
#include <list.h>

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
bool install_page (void *, void *, bool);

struct zombie{
	tid_t tid; //살아있을적 번호
	int cause_of_death; //사인
	struct list_elem elem;
};

struct thread* process_find_child(tid_t);
struct zombie* process_find_zombie_child(tid_t);

struct zombie* zombie_create(tid_t, int);
#endif /* userprog/process.h */

