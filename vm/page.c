#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/vaddr.h"
#include "threads/thread.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "vm/page.h"
#include "vm/frame.h"
#include "devices/disk.h"
#include "filesys/file.h"
#include <string.h>

struct hash*
page_table_create()
{
	struct hash* page_table = (struct hash*)malloc(sizeof(struct hash));
	hash_init(page_table, page_table_hash_function, page_table_less_function, NULL);

	return page_table;
}

void
page_table_destroy(struct hash* page_table)
{
	hash_destroy(page_table, page_table_destroy_help_function);
	free(page_table);
}

void page_table_destroy_help_function (struct hash_elem *e, void *aux)
{
	struct supple_page *temp_page;

	temp_page = hash_entry(e, struct supple_page, elem);
	if(temp_page->stat == SWAP)
		swap_free(temp_page->swap_sector, NULL);

	free(temp_page);
}

unsigned
page_table_hash_function(const struct hash_elem *e, void *aux){
	return hash_int((int)hash_entry(e, struct supple_page, elem)->page);
}

bool
page_table_less_function(const struct hash_elem *a, const struct hash_elem *b, void *aux){
	return hash_entry(a, struct supple_page, elem)->page < hash_entry(b, struct supple_page, elem)->page;
}

bool page_table_set_frame(struct hash* hash_table, void *input_page)
{
	struct supple_page *temp_page;

	temp_page = malloc(sizeof(struct supple_page));
	temp_page->page = input_page;
	temp_page->stat = FRAME;
	temp_page->file = NULL;
	temp_page->bit_dirty = false;

	if(hash_insert(hash_table, &temp_page->elem) == NULL)
		return true;
	else
	{
		free(temp_page);
		return false;
	}
}

bool page_table_set_zero(struct hash* hash_table, void *input_page)
{
	struct supple_page *temp_page;

	temp_page = malloc(sizeof(struct supple_page));
	temp_page->page = input_page;
	temp_page->stat = ZERO;
	temp_page->file = NULL;
	temp_page->bit_dirty = false;

	if(hash_insert(hash_table, &temp_page->elem) == NULL)
		return true;
	else
	{
		free(temp_page);
		return false;
	}
}

bool page_table_set_swap(struct hash* hash_table, void *input_page, disk_sector_t input_sector, bool dirty)
{
	struct supple_page temp_page;
	struct supple_page *load_page;
	struct hash_elem *h;

	temp_page.page = input_page;

	h = hash_find(hash_table, &temp_page.elem);

	if(h == NULL)
		return false;

	load_page = hash_entry(h, struct supple_page, elem);

	load_page->stat = SWAP;
	load_page->swap_sector = input_sector;
	load_page->bit_dirty = load_page->bit_dirty || dirty;

	return true;
}

bool page_table_set_file(struct hash* hash_table, void *page, struct file *input_file, off_t offset, uint32_t read_byte, uint32_t zero_byte, bool writable)
{
	struct supple_page* temp_page;
	temp_page = malloc(sizeof(struct supple_page));
	temp_page->page = page;
	temp_page->stat = FILE;
	temp_page->file = input_file;
	temp_page->offset = offset;
	temp_page->read_bytes = read_byte;
	temp_page->zero_bytes = zero_byte;
	temp_page->writable = writable;
	temp_page->bit_dirty = false;

	if(hash_insert(hash_table, &temp_page->elem) == NULL)
	{
		return true;
	}
	else
	{
		free(temp_page);
		return false;
	}
}

bool page_table_is_in_table(struct hash* hash_table, void *page)
{
	struct supple_page temp_page;
	struct hash_elem *e;

	temp_page.page = page;
	e = hash_find(hash_table, &temp_page.elem);
	if(e != NULL)
		return true;
	else
		return false;
}

bool page_table_load_page(struct hash* hash_table, uint32_t *pagedir, void *page)
{
	// printf("pagedir : %d\n", pagedir);
	// printf("page    : %d\n", page);
	struct supple_page temp_page;
	struct supple_page *load_page;
	struct hash_elem *h; 
	// printf("load 1\n");
	temp_page.page = page;
	h = hash_find(hash_table, &temp_page.elem);
	load_page = hash_entry(h, struct supple_page, elem);
	switch(load_page->stat)
	{
		case ZERO:
		{
			// printf("load 2\n");
			uint8_t *kpage = frame_get_frame(PAL_USER, page);

			if(kpage == NULL)
				return false;

			memset(kpage, 0, PGSIZE);
			load_page->stat = FRAME;
			pagedir_set_page(pagedir, page, kpage, true);
			pagedir_set_dirty(pagedir, kpage, false);
			frame_unpin(kpage);

			break;

		}
		case SWAP:
		{
			// printf("load 3\n");
			uint8_t *kpage = frame_get_frame(PAL_USER, page);
			if(kpage == NULL)
				return false;

			swap_free(load_page->swap_sector, kpage);
			load_page->stat = FRAME;
			pagedir_set_page(pagedir, page, kpage, true);
			pagedir_set_dirty(pagedir, kpage, false);
			frame_unpin(kpage);
			break;

		}
		case FILE:
		{
			// printf("load 4\n");
			int read_byte;
			uint8_t* kpage = frame_get_frame(PAL_USER, page);
			if(kpage == NULL)
				return false;
			// printf("load 5\n");
			file_seek(load_page->file, load_page->offset);
			// int a =file_read(load_page->file, kpage, load_page->read_bytes);
			// int b =(int) load_page->read_bytes; 
			read_byte = file_read(load_page->file, kpage, load_page->read_bytes);
			if(read_byte != (int) load_page->read_bytes)
			{
			// printf("%d\n",a);
			// printf("%d\n",b);
			// if(a!=b){
				// printf("load5.5\n");
				frame_free(kpage);
				return false;
			}
			// printf("load 6\n");
			memset(kpage + load_page->read_bytes, 0, load_page->zero_bytes);
			load_page->stat = FRAME;
			// printf("load 7\n");
			// printf("-----------------------\n");
			// printf("pagedir : %d\n", pagedir);
			// printf("page    : %d\n", page);
			// printf("kpage   : %d\n", kpage);
			pagedir_set_page(pagedir, page, kpage, load_page->writable);
			// hex_dump((int*)0x10000000, (int*)0x10000000, 300, true);
			pagedir_set_dirty(pagedir, kpage, false);
			// printf("load 8\n");
			frame_unpin(kpage);
			break;
		}
		case FRAME:
		{
			break;
		}
	}

	return true;
}

void page_table_munmap(struct hash* page_table, uint32_t* pagedir, struct file* input_file, void* start, void* end)
{
	struct supple_page temp_page;
	struct supple_page* load_page;
	struct hash_elem *e;
	off_t offset = 0;

	file_seek(input_file, 0);
	// printf("pt_munmap 1\n");
	// printf("start : %d\n", start);
	// printf("e n d : %d\n", end);
	for (temp_page.page = start; temp_page.page < end; temp_page.page += PGSIZE, offset += PGSIZE)
	{
		// printf("page %d\n", temp_page.page);
		// printf("pt_munmap 1.3\n");
		
		frame_load_pin_set(temp_page.page, pagedir);
		// printf("pt_munmap 1.4\n");
		e = hash_find(page_table, &temp_page.elem);
		if(!e)
			continue;
		// printf("pt_munmap 1.5\n");
		load_page = hash_entry(e, struct supple_page, elem);
		// printf("pt_munmap 1.8\n");
		int size = end - temp_page.page;
		if(size > PGSIZE)
			size = PGSIZE;
		// printf("pt_munmap 2\n");
		// printf("load_page->stat : %d", load_page->stat);
		switch(load_page->stat)
		{
			case FRAME:
			{
				// printf("pt_munmap 3\n");
				if(load_page->bit_dirty || pagedir_is_dirty(pagedir, load_page->page) || pagedir_is_dirty(pagedir, pagedir_get_page(pagedir, load_page->page)))
					file_write_at(input_file, load_page->page, size, offset);
				frame_free(pagedir_get_page(pagedir, load_page->page));
				// printf("pt_munmap 4\n");
				pagedir_clear_page(pagedir, load_page->page);
				break;
			}
			case SWAP:
			{
				if(load_page->bit_dirty)
				{
					void* page = palloc_get_page(0);
					swap_free(load_page->swap_sector, page);
					file_write_at(input_file, page, size, offset);
					palloc_free_page(page);
				}
				else
					swap_free(load_page->swap_sector, NULL);

				break;
			}
			case FILE:
			{
				break;
			}
			case ZERO:
			{
				break;
			}
		}
		hash_delete(page_table, &load_page->elem);
		free (load_page);
	}
	file_close(input_file);
}
