#ifndef VM_SWAP_H
#define VM_SWAP_H

#include "devices/disk.h"

void swap_init(void);
disk_sector_t swap_assign(void *);
void swap_free(disk_sector_t, void *);

#endif