#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <hash.h>
#include "filesys/file.h"
#include "vm/frame.h"
#include "devices/disk.h"

enum page_status 
{
 FRAME,
 SWAP,
 ZERO,
 FILE
};

struct supple_page
{
	void* page;
	off_t offset;
	enum page_status stat;
	struct hash_elem elem;

	struct file *file;
	int read_bytes;
	int zero_bytes;
	bool writable;

	disk_sector_t swap_sector;

	bool bit_dirty;
};

struct hash* page_table_create(void);

void page_table_destroy(struct hash*);

void page_table_destroy_help_function (struct hash_elem*, void*);
unsigned page_table_hash_function(const struct hash_elem*, void*);
bool page_table_less_function(const struct hash_elem*, const struct hash_elem*, void*);

bool page_table_is_in_table(struct hash*, void*);
bool page_table_page_load(struct hash* , uint32_t*, void*);
bool page_table_set_frame(struct hash* , void *);
bool page_table_set_zero(struct hash* , void *);
bool page_table_set_swap(struct hash* , void *, disk_sector_t , bool);
bool page_table_set_file(struct hash*, void*, struct file*, off_t , uint32_t , uint32_t , bool);
bool page_table_load_page(struct hash*, uint32_t *, void *);
void page_table_munmap(struct hash*, uint32_t*, struct file*, void*, void*);

#endif


