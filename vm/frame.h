#ifndef VM_FRAME_H
#define VM_FRAME_H

#include "threads/palloc.h"
#include "threads/synch.h"
#include "vm/page.h"
#include <hash.h>
#include <stdbool.h>
#include <stdint.h>
#include <list.h>

struct frame
{
	unsigned frame;
	void* page;
	bool pinned;

	struct thread *thread;
	struct hash_elem elem;
};

void frame_init(void);
unsigned frame_hash_func(const struct hash_elem *, void *);
bool frame_less_func(const struct hash_elem *, const struct hash_elem *, void *);
void* frame_get_frame(enum palloc_flags , void *);
void frame_free(void *);
void frame_unpin(void *);
int frame_evict(void);
void frame_load_pin_set(void*, uint32_t*);

#endif
