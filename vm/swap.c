#include "vm/swap.h"
#include "lib/kernel/bitmap.h"
#include "devices/disk.h"
#include "threads/vaddr.h"

struct disk *swap_disk;
struct bitmap *swap_map;

void swap_init()
{
	swap_disk = disk_get(1, 1);
	swap_map = bitmap_create(disk_size(swap_disk));
}

disk_sector_t swap_assign(void *page)
{
	disk_sector_t assign = bitmap_scan_and_flip(swap_map, 0, PGSIZE/DISK_SECTOR_SIZE, 0);

	int i;
	for (i = assign; i < assign + PGSIZE/DISK_SECTOR_SIZE; i++)
	{
		disk_write(swap_disk, i, page);
		page += DISK_SECTOR_SIZE;
	}
	return assign;
}

void swap_free(disk_sector_t input, void *page)
{
	int i;

	if(page != NULL)
	{
		for (i = 0; i < PGSIZE/DISK_SECTOR_SIZE; i++)
		{
			disk_read(swap_disk, input+i, page);
			page += DISK_SECTOR_SIZE;
		}
	}

	bitmap_set_multiple(swap_map, input, PGSIZE/DISK_SECTOR_SIZE, 0);
}