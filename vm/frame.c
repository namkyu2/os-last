#include "vm/frame.h"
#include "vm/swap.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include <list.h>
#include "userprog/pagedir.h"
#include "devices/disk.h"
#include "lib/kernel/bitmap.h"

static struct lock frame_lock;
static struct hash frame_table;

void frame_real_free(void *);


void frame_init()
{
	lock_init(&frame_lock);
	hash_init(&frame_table, frame_hash_func, frame_less_func, NULL);
}

unsigned frame_hash_func(const struct hash_elem *input_elem, void *aux)
{
  return hash_int((int) hash_entry(input_elem, struct frame, elem)->frame);
}

bool frame_less_func(const struct hash_elem *a, const struct hash_elem *b, void *aux)
{
  return hash_entry(a, struct frame, elem)->frame < hash_entry(b, struct frame, elem)->frame;
}

void* frame_get_frame(enum palloc_flags flags, void *upage)
{
	void *alloc;
	struct frame temp_frame;
	struct frame *f;
	struct hash_elem *h;
	struct frame *insert_frame;
	bool page_dirty;
	disk_sector_t sec;

	lock_acquire(&frame_lock);

	// printf("checkpoint3\n");

	alloc = palloc_get_page(PAL_USER|flags);
	while(alloc == NULL)
	{
		int evict = frame_evict();
		if(evict == -1)
		{
			alloc = palloc_get_page(PAL_USER|flags);
			continue;
		}
		temp_frame.frame = evict;

		h = hash_find(&frame_table, &temp_frame.elem);
		f = hash_entry(h, struct frame, elem);

		lock_acquire(&f->thread->pagedir_lock);
		if(f->thread->pagedir == NULL)
		{
			lock_release(&f->thread->pagedir_lock);
			continue;
		}

		page_dirty = pagedir_is_dirty(f->thread->pagedir, f->page) || pagedir_is_dirty(f->thread->pagedir, pagedir_get_page(f->thread->pagedir, f->page));

		pagedir_clear_page(f->thread->pagedir, f->page);
		lock_release(&f->thread->pagedir_lock);

		sec = swap_assign(ptov(f->frame));

		page_table_set_swap(f->thread->page_table, f->page, sec, page_dirty);

		frame_real_free(ptov(f->frame));

		alloc = palloc_get_page(PAL_USER|flags);
	}

	lock_release(&frame_lock);

	uintptr_t p = vtop(alloc);

	insert_frame = malloc(sizeof(struct frame));
	insert_frame->frame = p;
	insert_frame->page = upage;
	insert_frame->thread = thread_current();
	insert_frame->pinned = true;

	lock_acquire(&frame_lock);
	hash_insert(&frame_table, &insert_frame->elem);
	lock_release(&frame_lock);

	return alloc;
}

void frame_free(void *addr)
{

	lock_acquire(&frame_lock);
	frame_real_free(addr);
	lock_release(&frame_lock);
}

void frame_real_free(void *addr)
{
	struct frame temp_frame;
	struct hash_elem *h;
	struct frame *find_frame;

	temp_frame.frame = vtop(addr);

	h = hash_find(&frame_table,&temp_frame.elem);

	find_frame = hash_entry(h, struct frame, elem);
	palloc_free_page(addr);

	hash_delete(&frame_table, &find_frame->elem);

	free(find_frame);
}

void frame_unpin(void *page)
{
	struct frame temp_frame;
	struct frame *load_frame;
	struct hash_elem *h;

	temp_frame.frame = vtop(page);

	lock_acquire(&frame_lock);
	h = hash_find(&frame_table, &temp_frame.elem);

	load_frame = hash_entry(h, struct frame, elem);
	load_frame->pinned = false;
	lock_release(&frame_lock);
}

int frame_evict()
{
	size_t size;
	struct hash_iterator iter;
	struct frame *temp_frame;
	int temp_evict;

	size = hash_size(&frame_table);

	if(size == 0)
		return -1;

	hash_first(&iter, &frame_table);

	while(hash_next(&iter))
	{
		temp_frame = hash_entry(hash_cur(&iter), struct frame, elem);
		if(!(temp_frame->pinned))
			return temp_frame->frame;
	}

	hash_first(&iter, &frame_table);
	while(hash_next(&iter))
	{
		temp_frame = hash_entry(hash_cur(&iter), struct frame, elem);
		if(!(temp_frame->pinned))
			return temp_frame->frame;
	}

	return -1;
}

void frame_load_pin_set(void* upage, uint32_t* pagedir)
{
	// printf("pin set 1\n");
	lock_acquire(&frame_lock);
	void *kpage = pagedir_get_page(pagedir, upage);
	// printf("pin set 2\n");
	if(kpage)
	{
		struct frame temp_frame;
		struct frame* load_frame;
		struct hash_elem *e;
		// printf("pin set 3\n");
		temp_frame.frame = vtop(kpage);
		// printf("pin set 4\n");
		e = hash_find(&frame_table, &temp_frame.elem);
		// printf("pin set 5\n");
		if (!e)
		{
			lock_release(&frame_lock);
			return;
		}
		load_frame = hash_entry(e, struct frame, elem);
		// printf("pin set 6\n");
		load_frame->pinned = true;
		// printf("pin set 7\n");
	}
	// printf("pin set 8\n");

	lock_release(&frame_lock);
}
