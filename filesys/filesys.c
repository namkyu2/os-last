#include "filesys/filesys.h"
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "filesys/file.h"
#include "filesys/free-map.h"
#include "filesys/inode.h"
#include "filesys/directory.h"
#include "threads/synch.h"

/* The disk that contains the file system. */
struct disk *filesys_disk;

static void do_format (void);

/* Initializes the file system module.
   If FORMAT is true, reformats the file system. */

struct cache_elem
{
  disk_sector_t sector;
  int *content;
  bool is_used;
  bool changed;
  bool is_access;
};

struct lock cache_lock;
struct cache_elem cache_array[64];
int clock;

void
filesys_init (bool format) 
{
  filesys_disk = disk_get (0, 1);
  if (filesys_disk == NULL)
    PANIC ("hd0:1 (hdb) not present, file system initialization failed");

  inode_init ();
  free_map_init ();
  buffer_cache_init();

  if (format) 
    do_format ();

  free_map_open ();
}

/* Shuts down the file system module, writing any unwritten data
   to disk. */
void
filesys_done (void) 
{
  free_map_close ();
  buffer_cache_free();
}

/* Creates a file named NAME with the given INITIAL_SIZE.
   Returns true if successful, false otherwise.
   Fails if a file named NAME already exists,
   or if internal memory allocation fails. */
bool
filesys_create (const char *name, off_t initial_size) 
{
  disk_sector_t inode_sector = 0;
  struct dir *dir = dir_open_root ();
  bool success = (dir != NULL
                  && free_map_allocate (1, &inode_sector)
                  && inode_create (inode_sector, initial_size)
                  && dir_add (dir, name, inode_sector));
  if (!success && inode_sector != 0) 
    free_map_release (inode_sector, 1);
  dir_close (dir);

  return success;
}

/* Opens the file with the given NAME.
   Returns the new file if successful or a null pointer
   otherwise.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
struct file *
filesys_open (const char *name)
{
  struct dir *dir = dir_open_root ();
  struct inode *inode = NULL;

  if (dir != NULL)
    dir_lookup (dir, name, &inode);
  dir_close (dir);
  return file_open (inode);
}

/* Deletes the file named NAME.
   Returns true if successful, false on failure.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
bool
filesys_remove (const char *name) 
{
  struct dir *dir = dir_open_root ();
  bool success = dir != NULL && dir_remove (dir, name);
  dir_close (dir); 

  return success;
}

/* Formats the file system. */
static void
do_format (void)
{
  printf ("Formatting file system...");
  free_map_create ();
  if (!dir_create (ROOT_DIR_SECTOR, 16))
    PANIC ("root directory creation failed");
  free_map_close ();
  printf ("done.\n");
}

void buffer_cache_init ()
{
  int i;
  for(i = 0; i < 64; i++)
  {
    cache_array[i].content = malloc(DISK_SECTOR_SIZE);
    cache_array[i].is_used = false;
    cache_array[i].is_access = false;
  }
  clock = 0;
  lock_init(&cache_lock);
}

void buffer_cache_free()
{
  int i;

  buffer_cache_flush();

  for (i = 0; i < 64; i ++)
    free(cache_array[i].content);

}

void buffer_cache_flush()
{
  int i;

  lock_acquire(&cache_lock);

  for(i = 0; i < 64; i++)
  {
    if(cache_array[i].changed)
    {
      disk_write(filesys_disk, cache_array[i].sector, cache_array[i].content);
      cache_array[i].changed = false;
    }
  }

  lock_release(&cache_lock);
}

void buffer_cache_read(disk_sector_t sector, int *content)
{
  buffer_cache_read_in_range(sector, content, 0, DISK_SECTOR_SIZE);
}

void buffer_cache_read_in_range(disk_sector_t sector, int *content, int offset, int size)
{
  int i;

  // lock_acquire(&cache_lock);

  for(i = 0; i < 64; i++)
  {
    if(cache_array[i].sector == sector || !cache_array[i].is_used)
      break;
  }

  if (i == 64)
    i = buffer_cache_evict();

  if (!cache_array[i].is_used)
  {
    cache_array[i].sector = sector;
    cache_array[i].changed = false;
    cache_array[i].is_used = true;
    disk_read(filesys_disk, sector, cache_array[i].content);
  }

  cache_array[i].is_access = true;
  memcpy(content, cache_array[i].content + offset, size);

  // lock_release(&cache_lock);

}
void buffer_cache_write(disk_sector_t sector, int *content)
{
  buffer_cache_write_in_range(sector, content, 0, DISK_SECTOR_SIZE);
}

void buffer_cache_write_in_range(disk_sector_t sector, int *content, int offset, int size)
{
  int i;

   lock_acquire(&cache_lock);

  for(i = 0; i < 64; i++)
  {
    if(cache_array[i].sector == sector || !cache_array[i].is_used)
      break;
  }

  if (i == 64)
    i = buffer_cache_evict();

  if (!cache_array[i].is_used)
  {
    cache_array[i].sector = sector;
    cache_array[i].is_used = true;
  }

  cache_array[i].changed = true;
  cache_array[i].is_access = true;
  if (content != NULL)
    memcpy(cache_array[i].content + offset, content, size);
  else
    memcpy(cache_array[i].content + offset, 0, size);

   lock_release(&cache_lock);
}

int buffer_cache_evict(void)
{
  while(true)
  {
    if(cache_array[clock].is_access)
      cache_array[clock].is_access = false;
    else
      break;

    clock += 1;

    if(clock > 64)
      clock = 0;
  }
  if(cache_array[clock].changed)
    disk_write(filesys_disk, cache_array[clock].sector, cache_array[clock].content);

  cache_array[clock].is_used = false;
  cache_array[clock].changed = false;
  return clock;
}


